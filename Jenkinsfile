@Library('hawaii') _ 

// Declarative pipeline
pipeline {
	agent any
	
    tools {
        jdk "JDK openjdk 1.8"
        maven "Maven 3.2.5"
    }

    // declaration des options (connexion gitlab, configuration...)
    options {
        disableConcurrentBuilds()
        gitLabConnection('gitlab-jenkins')
        gitlabBuilds(builds: ['test', 'coverage', 'quality'])
    }

    triggers {
        gitlab(
          triggerOnPush: true,
          triggerOnMergeRequest: true,
          skipWorkInProgressMergeRequest: true,
          triggerOpenMergeRequestOnPush: 'both',
          acceptMergeRequestOnSuccess: false,
          branchFilterType: "RegexBasedFilter",
          targetBranchRegex: ".*release.*|.*feature.*|.*hotfix.*"
        )
    }

    stages {
	
		stage('test') {
            steps {
                utilsExecuteCommand(cmd: "mvn -U clean test")
                updateGitlabCommitStatus name: 'test', state: 'success'
            }
        }

        stage('coverage') {
            steps {
                utilsExecuteCommand(cmd: "mvn org.jacoco:jacoco-maven-plugin:prepare-agent test")
				junit "target/surefire-reports/*.xml"
                updateGitlabCommitStatus name: 'coverage', state: 'success'
            }
        }

        stage('quality') {
                steps {
                    script {
                        // add GIT_BRANCH to sonar analysis
                        def sonarBranch = ''
                        if ( GIT_BRANCH.trim().contains('origin/master')) {
                            sonarBranch = "master"
                        } else {
                            sonarBranch = GIT_BRANCH.replace("origin/","")
                        }
    
                        withSonarQubeEnv('Sonar LTS') {
                            utilsExecuteCommand(cmd: "mvn sonar:sonar -Dsonar.branch.name=${sonarBranch}")
                        }
                        // Check Quality Gate
                        timeout(time: 10) {
                            sleep(10)
                            def qualitygate = waitForQualityGate()
                            if (qualitygate.status != "OK") {
                                error "Pipeline aborted due to quality gate coverage failure: ${qualitygate.status}"
                            }
                        }
                    }
                updateGitlabCommitStatus name: 'quality', state: 'success'
            }
        }
		
		stage('build') {
            steps {
                utilsExecuteCommand(cmd: "mvn package -Dmaven.test.skip=true")
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }

        stage('upload'){
			when { 
				expression { env.GIT_BRANCH?.startsWith('origin/release') || env.GIT_BRANCH?.startsWith('origin/hotfix') }
			}
            steps{
                script {
					utilsExecuteCommand(cmd: "mvn resources:resources && cp -f target/hawaiifile.yaml hawaiifile.yml")
                    hawaiiUploadRest()
                    gitlabSetTag()
                }
                updateGitlabCommitStatus name: 'upload', state: 'success'
            }
        }
    
        stage('deploy'){
			when { 
				expression { env.GIT_BRANCH?.startsWith('origin/release')}
			}
            steps{
                script {
                    hawaiiDeployRest()
                }
                updateGitlabCommitStatus name: 'deploy', state: 'success'
            }
        }
		
		stage('deployMac'){
            when { 
                expression { env.GIT_BRANCH?.startsWith('origin/hotfix') }
            }
            steps{
                hawaiiDeployRest(environment: "MAC")
                updateGitlabCommitStatus name: 'deployMac', state: 'success'
            }
        }
        
    }
}